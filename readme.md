# Godot Todo List

Todo list implementation as an exercise in getting familiar with GDScript

![Preview](preview.gif)

Example serialised todo file in `test-teodos-v2.json`.

## References

* https://docs.godotengine.org/en/stable/getting_started/scripting/gdscript/
* https://github.com/godotengine/godot/issues/17268
