extends HBoxContainer

signal todo_deleted
signal todo_status_updated

var text
var done

func init(_text, _done):
	text = _text
	done = _done

# Called when the node enters the scene tree for the first time.
func _ready():
	$CheckBox.pressed = self.done
	render_text()
	
func render_text():
	$Task.clear()
	
	if done:
		$Task.push_strikethrough()
		
	$Task.add_text(self.text)
	
	if done:
		$Task.pop()

func update_status(_done):
	self.done = _done
	self.render_text()
	emit_signal('todo_status_updated')

func _on_CheckBox_toggled(button_pressed):
	update_status(button_pressed)


func _on_DeleteButton_pressed():
	emit_signal('todo_deleted', self)
