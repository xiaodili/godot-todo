extends Control

const Util = preload('utils.gd')

export(PackedScene) var TodoItem

# Called when the node enters the scene tree for the first time.
func _ready():
	render_info()
	
func get_todo_nodes():
	return $VBoxContainer/Todos.get_children()
	
func todo_is_done_p(todo):
	return todo.done

func render_info():
	var todos = self.get_todo_nodes()
	var num_todos = todos.size()
	var num_done = Util.filter(funcref(self, 'todo_is_done_p'), todos).size()
	$VBoxContainer/HBoxContainer/Info.text = '%s/%s tasks done' % [num_done, num_todos]

func remove_todo(todo):
	$VBoxContainer/Todos.remove_child(todo)
	self.render_info()
	todo.queue_free()


func add_todo(text, done):
	var todo_item = TodoItem.instance()
	todo_item.init(text, done)
	$VBoxContainer/Todos.add_child(todo_item)
	todo_item.connect("todo_status_updated", self, "render_info")
	todo_item.connect("todo_deleted", self, "remove_todo")
	render_info()

func _on_NewTodo_new_todo(text):
	add_todo(text, false)

func _on_LoadButton_pressed():
	$LoadFileDialog.popup()

func _on_SaveButton_pressed():
	$SaveFileDialog.popup()

func load_todos(_todos):
	
	for todo in self.get_todo_nodes():
		$VBoxContainer/Todos.remove_child(todo)
		todo.queue_free()
	
	for todo in _todos:
		add_todo(todo.task, todo.done)

func _on_FileDialog_file_selected(path):
	var file = File.new()
	file.open(path, File.READ)
	var content = file.get_as_text()
	var todos_parsed = JSON.parse(content)
	self.load_todos(todos_parsed.result)

func serialise_todos():
	var todos = self.get_todo_nodes()
	var todos_serialised = []
	for todo in todos:
		todos_serialised.append({
			task = todo.text,
			done = todo.done
		})
	return todos_serialised

func _on_SaveFileDialog_file_selected(path):
	var file = File.new()
	
	if file.open(path, File.WRITE) != 0:
		print("Error opening file")
		return
	
	file.store_line(JSON.print(self.serialise_todos()))
	file.close()
	
	$Popup.dialog_text = 'Tasks saved to %s' % path
	$Popup.show()
